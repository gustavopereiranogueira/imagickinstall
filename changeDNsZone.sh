#!/bin/bash

##############################################################################
#																			 
# Script para alterar endereços de DNS após alteração de domínio primário:
#
# Autor: Gustavo Nogueira
#
# Data: 08/06/2020
#
###############################################################################

### Setando Variáveis ###
BOLD="\e[1m"
RED="\e[31m"
YELLOW="\e[33m"
GREEN="\e[32m"
BLUE="\e[34m"
RED_ALERT="\e[41m"

## Executa o Script somente em servidores DEDICADOS/VPS

if [[ ! -z $(hostname | egrep 'hostgator.com.br|prodns' ) ]]
then
        echo -e "${negrito}Esse script ${negrito}${vermelho}não deve${reset}${negrito} ser executado em servidores compartilhados!"

        exit 1
fi

### Verificando se o domínio existe no servidor antes de alterar o domínio Primário ###

read -p "Verifique se o domínio existe no servidor: " DOMAIN

if [[ $(grep ${DOMAIN} /etc/trueuserdomains | cut -d: -f1) ]]

then
        echo "O domínio Existe. Não possível alterar o domínio primário..."

else
        echo "O domínio não existe!"

fi



## Amazenando domínio antigo na variável ##

echo -n  "Digite o domínio antigo a ser substituído: "
        read OLDOMAIN

if [[ ${OLDOMAIN} ]]
then
        echo "Infome o antigo domínio"

else
        echo "O domínio ${OLDOMAIN} foi informado"

fi

echo -n "Digite o domínio atual a ser corrido: "
        read NEWDOMAIN

if [[ ${NEWDOMAIN} ]]
then

        echo "Informe o novo Domínio a ser substituído: "
else

        echo "O novo domínio ${NEWDOMAIN} foi informado"

fi

if [[ ${OLDOMAIN} || ${NEWDOMAIN} ]]
then
        echo "O domínio ${OLDOMAIN} será substituido pelo domínio ${NEWDOMAIN} na zona de DNS de TODOS os domínios em  /var/named !!!"

sleep 2

else
        for i in $(cat /etc/trueuserdomains | cut -d: -f1); do sed -i 's/ns1.${OLDOMAIN}/ns1.${NEWDOMAIN}/g' /var/named/$i.db ; 
        sed -i 's/ns2.${OLDOMAIN}/ns2.${NEWDOMAIN}/g' /var/named/$i.db ; done


fi
